package com.ciandtjava.formwebspring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FormwebSpringApplication {

	public static void main(String[] args) {

		SpringApplication.run(FormwebSpringApplication.class, args);
	}

}
